var gulp = require('gulp'),
    fs = require('fs-extra'),
    jsdoc = require('gulp-jsdoc3');

gulp.task('default', function() {
    // Clean dist/
    fs.removeSync('dist');

    // Move HTML
    gulp.src('src/*.html')
        .pipe(gulp.dest('dist/'));

    // Move JS
    gulp.src('src/js/*.js')
        .pipe(gulp.dest('dist/js/'));

    // Move CSS
    gulp.src('src/css/*.css')
        .pipe(gulp.dest('dist/css/'));

    // Move Images
    gulp.src('src/img/*.png')
        .pipe(gulp.dest('dist/img/'));

    // Move Phaser
    gulp.src(['node_modules/phaser-ce/build/phaser.js', 'node_modules/phaser-ce/build/phaser.map'])
        .pipe(gulp.dest('dist/js/'));

    // Move RequireJS
    gulp.src('node_modules/requirejs/require.js')
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('jsdocs', function(cb) {
    gulp.src(['README.md', 'src/js/*.js'], {read: false})
        .pipe(jsdoc(cb));
});

gulp.task('watch', function() {
    console.log('Watcher loaded. Waiting for changes...'); // eslint-disable-line
    var watcher = gulp.watch('src/**/*', ['default']);
    watcher.on('change', function(/* event */) {
        console.log('Changes detected, running default task...'); // eslint-disable-line
    });
});