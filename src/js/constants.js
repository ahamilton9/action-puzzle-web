/**
 * Constants object
 * @module constants
 */
define([], function constants() {
    'use strict';

    return {
        CONTROLS: {
            CURSOR: {
                MAX: {
                    TOP: 22,
                    BOTTOM: 198,
                    LEFT: 86,
                    RIGHT: 146
                },
                INTERVAL: 16
            },

            // Leaving high for now. Somewhere between 50 and 100 is ideal
            LOCK_TIMEOUT: 100
        }
    };
});