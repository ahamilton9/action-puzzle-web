/**
 * Loaded by RequireJS. Chain-loads the game module
 * @function main
 * @see module:game
 */
(function main() {
    'use strict';

    // Configure RequireJS, shim Phaser
    require.config({
        paths: {
            phaser: 'phaser'
        },
        shim: {
            phaser: {
                exports: 'Phaser'
            }
        }
    });

    // Chain-load the game module
    require(['game']);
}());