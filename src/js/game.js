define([
    'phaser',
    'constants',
    'logger'
], function game(
    Phaser,
    CONSTANTS, // eslint-disable-line
    Logger
) {
    'use strict';

    /**
     * Loads Phaser and initializes the game
     * @exports game
     * @see module:logger
     * @see module:controls
     */
    var game = new Phaser.Game(
        256,
        224,
        Phaser.AUTO,
        '',
        {
            /**
             * Phaser preload function
             */
            preload: function preload() {
                // Background
                game.load.image('bg', 'img/bg.png');

                // Sprites
                game.load.image('cursor', 'img/cursor.png');
            },
            /**
             * Phaser create function
             */
            create: function create() {
                // Sprites
                game.add.sprite(0, 0, 'bg');

                // Game is ready, initialize other modules
                require([
                    'controls'
                ], function modulesLoaded(
                    controls
                ) {
                    var logger = new Logger('game');
                    logger.info('Modules Loaded');

                    /**
                     * Phaser update function
                     * @function update
                     */
                    game.state.update = function update() {
                        controls.update();
                    };
                });
            }
        }
    );

    return game;
});