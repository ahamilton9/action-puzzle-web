define([
    'game',
    'constants'
], function controls(
    game,
    CONSTANTS
) {
    'use strict';

    /**
     * Input and controller management
     * @exports controls
     * @see module:game
     */
    var Controls = function Controls() {
        var self = this,
            controllerMappings,
            arrows,
            cursor,
            gamepad,

            // Flags
            controlLock = {
                // Player is holding a key down
                up: false,
                down: false,
                left: false,
                right: false
            },

            // Timers
            controlLockTime = {
                // Player started holding a key down
                up: 0,
                down: 0,
                left: 0,
                right: 0
            };

        /**
         * Gamepad connection logic
         * @function gamepadConnected
         */
        window.addEventListener('gamepadconnected', function gamepadConnected(e) {
            if (e.gamepad.mapping === 'standard') {
                gamepad = e.gamepad;
                controllerMappings = {
                    up: gamepad.buttons[12],
                    down: gamepad.buttons[13],
                    left: gamepad.buttons[14],
                    right: gamepad.buttons[15]
                };
            } else {
                console.log('Unrecognized Controller. Dumping:'); // eslint-disable-line
                console.log(e.gamepad); // eslint-disable-line
            }
        });

        /**
         * Logs the cursor's current position. For debug purposes
         */
        function cursorPos() {
            // console.log('Cursor Position', [cursor.x, cursor.y]);
        }

        /**
         * Check if enough time has passed to move again. Spam prevention
         * @param {string} direction Direction the user is holding down
         * @returns {boolean} Whether enough time has passed
         */
        function moveTimeHasPassed(direction) {
            return (Date.now() - controlLockTime[direction]) > CONSTANTS.CONTROLS.LOCK_TIMEOUT;
        }

        /**
         * Controls module update function. Called by the Phaser update loop
         * @function update
         */
        self.update = function update() {
            // Clear controller mappings if moving back to keyboard
            // This currently acts as the input type flag
            if (
                typeof controllerMappings !== 'undefined' &&
                (
                    arrows.up.isDown ||
                    arrows.down.isDown ||
                    arrows.left.isDown ||
                    arrows.right.isDown
                )
            ) {
                controllerMappings = undefined;
            }

            // Move up
            if (
                (controllerMappings === undefined ? arrows.up.isDown : controllerMappings.up.pressed) &&
                cursor.y > CONSTANTS.CONTROLS.CURSOR.MAX.TOP
            ) {
                if (controlLock.up === false) {
                    cursor.y = cursor.y - CONSTANTS.CONTROLS.CURSOR.INTERVAL;
                    controlLock.up = true;
                    controlLockTime.up = Date.now();
                }
                cursorPos();
            }

            // Move down
            if (
                (controllerMappings === undefined ? arrows.down.isDown : controllerMappings.down.pressed) &&
                cursor.y < CONSTANTS.CONTROLS.CURSOR.MAX.BOTTOM
            ) {
                if (controlLock.down === false) {
                    cursor.y = cursor.y + CONSTANTS.CONTROLS.CURSOR.INTERVAL;
                    controlLock.down = true;
                    controlLockTime.down = Date.now();
                }
                cursorPos();
            }

            // Move left
            if (
                (controllerMappings === undefined ? arrows.left.isDown : controllerMappings.left.pressed) &&
                cursor.x > CONSTANTS.CONTROLS.CURSOR.MAX.LEFT
            ) {
                if (controlLock.left === false) {
                    cursor.x = cursor.x - CONSTANTS.CONTROLS.CURSOR.INTERVAL;
                    controlLock.left = true;
                    controlLockTime.left = Date.now();
                }
                cursorPos();
            }

            // Move right
            if (
                (controllerMappings === undefined ? arrows.right.isDown : controllerMappings.right.pressed) &&
                cursor.x < CONSTANTS.CONTROLS.CURSOR.MAX.RIGHT
            ) {
                if (controlLock.right === false) {
                    cursor.x = cursor.x + CONSTANTS.CONTROLS.CURSOR.INTERVAL;
                    controlLock.right = true;
                    controlLockTime.right = Date.now();
                }
                cursorPos();
            }

            // Reset control lock
            if (controlLock.up && (controllerMappings === undefined ? arrows.up.isUp : !controllerMappings.up.pressed) && moveTimeHasPassed('up')) {controlLock.up = false;}
            if (controlLock.down && (controllerMappings === undefined ? arrows.down.isUp : !controllerMappings.down.pressed) && moveTimeHasPassed('down')) {controlLock.down = false;}
            if (controlLock.left && (controllerMappings === undefined ? arrows.left.isUp : !controllerMappings.left.pressed) && moveTimeHasPassed('left')) {controlLock.left = false;}
            if (controlLock.right && (controllerMappings === undefined ? arrows.right.isUp : !controllerMappings.right.pressed) && moveTimeHasPassed('right')) {controlLock.right = false;}
        };

        /**
         * Run immediately on instantiation
         * @function init
         */
        self.init = function init() {
            arrows = game.input.keyboard.createCursorKeys();
            cursor = game.add.sprite(CONSTANTS.CONTROLS.CURSOR.MAX.LEFT, CONSTANTS.CONTROLS.CURSOR.MAX.BOTTOM, 'cursor');
        };
        self.init();

        return self;
    };

    return new Controls();
});