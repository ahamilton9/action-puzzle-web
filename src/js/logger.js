/* eslint no-console: 0 */
/**
 * Simple wrapper to make logging a bit more helpful
 * @module logger
 */
define(function logger() {
    'use strict';

    /**
     * Logger factory
     * @class
     * @alias module:logger
     */
    var Logger = function Logger(name) {
        var self = this,
            header = '%c [' + name + '] %c',
            fontColor = 'color: #fff;',
            infoStyles = 'background: #203ea0;' + fontColor,
            errorStyles = 'background: #a01f1f;' + fontColor,
            successStyles = 'background: #1ea031;' + fontColor;

        /**
         * Logs header in blue, for information
         * @function info
         */
        self.info = function info(message) {
            console.log(header + message, infoStyles);
        };

        /**
         * Logs header in red, for errors
         * @function error
         */
        self.error = function error(message) {
            console.log(header + message, errorStyles);
        };

        /**
         * Logs header in green, for successful debug cases
         * @function success
         */
        self.success = function success(message) {
            console.log(header + message, successStyles);
        };
    };

    return Logger;
});