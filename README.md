# action-puzzle-web

A simple in-browser action puzzle game

* You can move a cursor around a blank screen with the arrow keys!
* An Xbox controller directional-pad can be used (syncs on first button press)!

## Pre-Requisites

* node + npm, [from here](https://nodejs.org/en/)
* gulp, via `npm install -g gulp-cli`

## Build

1. `cd action-puzzle-web/`
2. `npm install`
3. `gulp`

## Play

Tested in Firefox, Chrome, and Opera

* For Firefox, just open `dist/index.html`
* For Chrome and Opera:
    * Use a script in `browsers/` to launch your choice with a flag allowing local AJAX calls
    * Drag `dist/index.html` into that window